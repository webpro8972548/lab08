import { log } from "console"
import { AppDataSource } from "./data-source"
import { User } from "./entity/User"
import { Role } from "./entity/Role"

AppDataSource.initialize().then(async () => {
    const usersRepository = AppDataSource.getRepository(User)
    const rolesRepository = AppDataSource.getRepository(Role)

    const adminRole = await rolesRepository.findOneBy({ id: 1})
    const userRole = await rolesRepository.findOneBy({ id: 2})

    await usersRepository.clear();
    console.log("Inserting a new user into the Memory...")
    var user = new User()
    user.id = 1
    user.email = "user1@gamil.com"
    user.gender = "male"
    user.password = "Pass"
    user.roles = []
    user.roles.push(adminRole)
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...")
    await usersRepository.save(user);

    //const admin = await usersRepository.findOneBy({ id:1 })
    //console.log(admin)

    user = new User()
    user.id = 2
    user.email = "user2@gamil.com"
    user.gender = "male"
    user.password = "Pass2"
    user.roles = []
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...")
    await usersRepository.save(user);

    //const user2 = await usersRepository.find({ where:{ id:2, gender:"male"} 
    //});
    //console.log(user2)

    user = new User()
    user.id = 3
    user.email = "user3@gamil.com"
    user.gender = "male"
    user.password = "Pass3"
    user.roles = []
    user.roles.push(userRole)
    console.log("Inserting a new user into the Database...")
    await usersRepository.save(user);

    //const user3 = await usersRepository.findOneBy({ id : 3 });
    //console.log(user3)

    const users = await usersRepository.find({ relations: { roles: true }})
    console.log(JSON.stringify(users, null, 2))

    const roles = await rolesRepository.find({ relations: { users: true }})
    console.log(JSON.stringify(roles, null, 2))


}).catch(error => console.log(error))
